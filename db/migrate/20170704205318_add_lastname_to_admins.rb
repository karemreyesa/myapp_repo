class AddLastnameToAdmins < ActiveRecord::Migration[5.0]
  def change
    add_column :admins, :lastname, :string
  end
end
