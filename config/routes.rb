Rails.application.routes.draw do
 
  devise_for :users, controllers:{
    sessions: 'users/sessions'
  }
   #nos dice donde estan las sessions
 devise_for :admins, controllers:{
    sessions: 'admins/sessions'
  }
  devise_scope :admin do
      get "admins/user/list",
      to: "admins/sessions#list",
      as: "list_user"
  end
  
  resources :zombies do
      resources :brains
  end

 # get "users/list", to: "zombies#user", as: "list_user"
    root to: 'zombies#index'
 # get '/sexto/cerebros',to: 'brains#index', as: 'brains'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  

end
